Rails.application.routes.draw do
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
  resources :csvs, except: [:edit, :update]
  resources :delivery_notes, only: [:index, :show, :destroy]
end
