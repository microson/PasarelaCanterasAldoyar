class CreateCsvs < ActiveRecord::Migration[7.0]
  def change
    create_table :csvs do |t|
      t.string :state, default: 'new'

      t.timestamps
    end
  end
end
