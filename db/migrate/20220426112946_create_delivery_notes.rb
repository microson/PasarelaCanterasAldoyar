class CreateDeliveryNotes < ActiveRecord::Migration[7.0]
  def change
    create_table :delivery_notes do |t|
      t.string :right_id
      t.string :left_id

      t.timestamps
    end
  end
end
