class CreateDeliveryNoteLines < ActiveRecord::Migration[7.0]
  def change
    create_table :delivery_note_lines do |t|
      t.string :description

      t.timestamps
    end
  end
end
