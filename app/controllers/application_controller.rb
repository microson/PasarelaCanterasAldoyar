class ApplicationController < ActionController::Base
  # Set the current language
  before_action :set_language

  private

  def set_language
    # Set the language via a query param
    if params[:locale].present? && params[:locale].to_sym.in?(I18n.available_locales)
      session[:locale] = params[:locale]
    end

    # Set the language via an HTTP header
    if session[:locale].blank? && request.env['HTTP_ACCEPT_LANGUAGE']
      session[:locale] = request.env['HTTP_ACCEPT_LANGUAGE'].scan(/^[a-z]{2}/).first
    end

    # If the session locale is valid, use it. Else, use the default locale
    if session[:locale].to_sym.in?(I18n.available_locales)
      I18n.locale = session[:locale]
    else
      session[:locale] = I18n.available_locales.first
    end
  end
end
