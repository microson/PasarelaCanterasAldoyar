class CsvsController < ApplicationController
  before_action :set_csv, only: %i[ show destroy ]

  def index
    @csvs = Csv
    if params[:search].present?
      query = "%#{params[:search]}%"
      @csvs = @csvs.where("name LIKE ?", query)
    end
    @csvs = @csvs.paginate(page: params[:page])
  end

  def new
    @csv = Csv.new
  end

  def create
    @csv = Csv.new(csv_params)

    respond_to do |format|
      if @csv.save
        format.html { redirect_to @csv, notice: "CSV was successfully created." }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @csv.destroy
    respond_to do |format|
      format.html { redirect_to csvs_url, notice: "CSV was successfully destroyed." }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_csv
      @csv = Csv.find(params[:id])
    end

    def csv_params
      params
        .require(:csv)
        .permit(
          :csv_file
        )
    end
end
